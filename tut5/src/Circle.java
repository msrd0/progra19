public class Circle
{
	private int x;
	private int y;
	private int radius;
	
	/**
	 * Erstellt einen neuen Kreis mit den angegebenen Werten. Wenn der radius negativ ist,
	 * wird ein Fehler zurückgegeben.
	 *
	 * @param x Die x-Koordinate des Mittelpunktes des Kreises.
	 * @param y Die y-Koordinate des Mittelpunktes des Kreises.
	 * @param radius Der Radius des Kreises.
	 * @return Der neu erstellte Kreis.
	 */
	public static Circle newCircle(int x, int y, int radius)
	{
		if (radius < 0)
		{
			Utils.error("negativer Radius");
			return null;
		}
		
		Circle circle = new Circle();
		circle.x = x;
		circle.y = y;
		circle.radius = radius;
		return circle;
	}
	
	public static Circle newCircle(int radius)
	{
		return newCircle(0, 0, radius);
	}
	
	public Circle clone()
	{
		return newCircle(this.x, this.y, this.radius);
	}
	
	public int getX()
	{
		return this.x;
	}
	
	public void setX(int x)
	{
		this.x = x;
	}
	
	public int getY()
	{
		return this.y;
	}
	
	public void setY(int y)
	{
		this.y = y;
	}
	
	public int getRadius()
	{
		return this.radius;
	}
	
	public void setRadius(int radius)
	{
		if (radius < 0)
			Utils.error("negativer Radius");
		else
			this.radius = radius;
	}
	
	public boolean contains(Circle ...other)
	{
		for (Circle circle : other)
		{
			if (Utils.dist(this.x, this.y, circle.x, circle.y) > this.radius - circle.radius)
				return false;
		}
		return true;
	}
	
	public static Circle circumscriber(int x, int y, Circle ...circles)
	{
		for (int radius = 1; true; radius++)
		{
			Circle circle = newCircle(x, y, radius);
			if (circle.contains(circles))
				return circle;
		}
	}
	
	public String toString()
	{
		// (10|11),12
		return "(" + this.x + "|" + this.y + ")," + this.radius;
	}
	
	public void performAction(CircleAction action)
	{
		switch(action)
		{
			case UP:
				this.y -= 10;
				break;
			case DOWN:
				this.y += 10;
				break;
			case LEFT:
				this.x -= 10;
				break;
			case RIGHT:
				this.x += 10;
				break;
			case BIGGER:
				this.radius += 10;
				break;
			case SMALLER:
				this.radius -= 10;
				break;
			default:
				break;
		}
	}
}
