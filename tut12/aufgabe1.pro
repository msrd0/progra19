increment(leaf(X), leaf(s(X))).
increment(node(L, N, R), node(IncL, s(N), IncR)) :- increment(L, IncL),
  increment(R, IncR).

myAppend([], YS, YS).
myAppend([X|XS], YS, [X|Res]) :- myAppend(XS, YS, Res).  

inorder(leaf(X), [X]).
inorder(node(L, N, R), Res) :- inorder(L, LRes), inorder(R, RRes),
  myAppend(LRes, [N|RRes], Res).
