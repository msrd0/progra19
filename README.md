# Progra WS 2019/20

Dieses Repository enthält Code, der während des Tutoriums entstanden ist. Solltet ihr Fehler finden, so weist mich bitte auf diese hin. Ich möchte ausdrücklich darauf hinweisen, dass es sich bei diesem Code nicht um die Musterlösung handelt; diese findet ihr im RWTHMoodle.
