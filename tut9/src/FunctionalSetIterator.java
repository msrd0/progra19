import java.util.*;

public class FunctionalSetIterator<E> implements Iterator<E>
{
	/** Unsere Menge */
	private FunctionalSet<E> set;
	/** Aktuelles Element */
	private SimpleFunctionalSet<E> current;
	/** Bereits gesehene Elemente */
	private Set<Object> seen = new FunctionalSet<>();
	/** Aktuelles Element */
	private E currentElem;
	/** Ob das aktuelle Element bereits entfernt wurde. */
	private boolean hasBeenRemoved = true;
	
	private void skipToNext()
	{
		while (true)
		{
			current = current.getRemainingSet();
			if (current instanceof RemoveSet)
				seen.add(((RemoveSet) current).getObject());
			else if (!(current instanceof AddSet) || !seen.contains(((AddSet)current).getElement()))
				break;
		}
	}
	
	public FunctionalSetIterator(FunctionalSet<E> set, SimpleFunctionalSet<E> head)
	{
		this.set = set;
		current = head;
		if (current instanceof RemoveSet)
		{
			seen.add(((RemoveSet)current).getObject());
			skipToNext();
		}
	}
	
	@Override
	public boolean hasNext()
	{
		return !(current instanceof EmptySet);
	}
	
	@Override
	public E next()
	{
		if (!hasNext())
			throw new NoSuchElementException();
		
		currentElem = ((AddSet<E>) current).getElement();
		seen.add(currentElem);
		skipToNext();
		hasBeenRemoved = false;
		return currentElem;
	}
	
	@Override
	public void remove()
	{
		if (hasBeenRemoved)
			throw new IllegalStateException("Das Element wurde schon entfernt");
		
		set.remove(currentElem);
		hasBeenRemoved = true;
	}
}
