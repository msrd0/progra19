import java.util.*;

public class Main
{
	public static void main(String args[])
	{
		FunctionalSet<Integer> s = new FunctionalSet<>();
		String line;
		do
		{
			line = SimpleIO.getString("Enter 'add i', 'remove i', 'min', or 'exit'!");
			String[] words = line.split(" ");
			switch (words[0])
			{
				case "exit":
					break;
				case "add":
					try
					{
						s.add(Integer.parseInt(words[1]));
						System.out.println(s);
					}
					catch (NumberFormatException nfe)
					{
						System.out.println("FEHLER: Eingabe ungültig!");
					}
					break;
				case "remove":
					try
					{
						s.remove(Integer.parseInt(words[1]));
						System.out.println(s);
					}
					catch (NumberFormatException nfe)
					{
						System.out.println("FEHLER: Eingabe ungültig!");
					}
					break;
				case "min":
					try
					{
						System.out.println(s.min(Comparator.naturalOrder()));
					}
					catch (MinimumOfEmptySetException moese)
					{
						System.out.println("FEHLER: Set enthaelt keine Elemente");
					}
					break;
				default:
					System.out.println("Unknown command.");
			}
		}
		while (!"exit".equals(line));
	}
}
