public class BoolTreeNode
{
	private String variable;
	private BoolTreeNode child1;
	private BoolTreeNode child2;
	
	private BoolTreeNode(String variableInput)
	{
		variable = variableInput;
	}
	
	private BoolTreeNode(BoolTreeNode negated)
	{
		child1 = negated;
	}
	
	private BoolTreeNode(BoolTreeNode kind1, BoolTreeNode kind2)
	{
		child1 = kind1;
		child2 = kind2;
	}
	
	public static BoolTreeNode boolTreeTrueNode()
	{
		return new BoolTreeNode("true");
	}
	
	public static BoolTreeNode boolTreeFalseNode()
	{
		return new BoolTreeNode("false");
	}
	
	public static BoolTreeNode boolTreeVariableNode(String variableInput)
	{
		if (variableInput == null || variableInput.length() == 0 || variableInput.equals("true") || variableInput.equals("false"))
		{
			Utils.error("ungueltiger Name der Variablen");
			return null;
		}
		
		return new BoolTreeNode(variableInput);
	}
	
	public static BoolTreeNode boolTreeNotNode(BoolTreeNode negated)
	{
		return new BoolTreeNode(negated);
	}
	
	public static BoolTreeNode boolTreeAndNode(BoolTreeNode child1, BoolTreeNode child2)
	{
		return new BoolTreeNode(child1, child2);
	}
	
	public int depth()
	{
		if (child2 != null) // konjunktionsknoten
		{
			int depth1 = child1.depth();
			int depth2 = child2.depth();
			return Utils.max(depth1, depth2) + 1;
		}
		
		if (child1 != null) // negationsknoten
		{
			return child1.depth() + 1;
		}
		
		// variablenknoten
		return 0;
	}
	
	public boolean isLeaf()
	{
		return (depth() == 0);
	}
	
	public boolean isTrueLeaf()
	{
		return isLeaf() && variable.equals("true");
	}
	
	public boolean isFalseLeaf()
	{
		return isLeaf() && variable.equals("false");
	}
	
	public boolean isNegation()
	{
		return (child1 != null && child2 == null);
	}
	
	public boolean isConjunction()
	{
		return (child1 != null && child2 != null);
	}
	
	/**
	 * Wertet den durch diesen Baum dargestellten Booleschen Ausdruck aus.
	 *
	 * @param trueVars Die Namen aller Variablen, die true sind.
	 * @return Den Wert des Ausdruckes.
	 */
	public boolean evaluate(String ...trueVars)
	{
		if (isLeaf()) // variablenknoten
			return Utils.evaluateVariable(variable, trueVars);
		
		if (isConjunction()) // konjunktionsknoten
			return child1.evaluate(trueVars) && child2.evaluate(trueVars);
		
		// negationsknoten
		return !child1.evaluate(trueVars);
	}
	
	/**
	 * Entfernt doppelte Negationen aus dem Baum.
	 *
	 * @return true wenn eine doppelte Negation gefunden wurde und entfernt wurde.
	 */
	public boolean removeDoubleNegations()
	{
		if (isLeaf())
			return false;
		if (isConjunction())
			return child1.removeDoubleNegations() | child2.removeDoubleNegations();
		
		// negationsknoten
		if (!child1.isNegation())
			return child1.removeDoubleNegations();
		BoolTreeNode grandchild = child1.child1;
		variable = grandchild.variable;
		child1 = grandchild.child1;
		child2 = grandchild.child2;
		child1.removeDoubleNegations();
		child2.removeDoubleNegations();
		return true;
	}
	
	public static void main(String args[])
	{
		BoolTreeNode tree = boolTreeAndNode(
				boolTreeNotNode(boolTreeNotNode(boolTreeVariableNode("a"))),
				boolTreeNotNode(boolTreeNotNode(boolTreeVariableNode("b"))));
		System.out.println(tree.removeDoubleNegations());
		System.out.println("fertig");
	}
}
