data Mobile a = Stern | Seepferdchen | Elefant (Mobile a)
                      | Kaenguru a (Mobile a) (Mobile a) deriving Show

mobileRechts = Elefant (Kaenguru 1 (Elefant (Stern)) (Elefant (Seepferdchen)))

mobileLinks = Kaenguru 1 (Elefant
                          (Kaenguru 2 (Stern) (Kaenguru 3 (Seepferdchen) (Stern)))
                         ) (Seepferdchen)

count :: Mobile a -> Int
count Stern = 1
count Seepferdchen = 1
count (Elefant x) = 1 + count x
count (Kaenguru _ y z) = 1 + count y + count z

liste :: Mobile a -> [a]
liste Stern = []
liste Seepferdchen = []
liste (Elefant x) = liste x
liste (Kaenguru x y z) = x : liste y ++ liste z

greife :: Mobile a -> Int -> Mobile a
greife x 1 = x
greife (Elefant x) i = greife x (i-1)
greife (Kaenguru _ x y) i | (i-1) <= count x = greife x (i-1)
                          | otherwise = greife y (i-1-(count x))
