from :: Int -> [Int]
from x = x : from (x+1)

-- take :: Int -> [a] -> [a]
-- take 0 _ = []
-- take n (x:xs) = x : take (n-1) xs

drop_mult :: Int -> [Int] -> [Int]
drop_mult x xs = filter (\y -> mod y x /= 0) xs

dropall :: [Int] -> [Int]
dropall (x:xs) = x : dropall (drop_mult x xs)

primes :: [Int]
primes = dropall (from 2)





odds :: [Int]
odds = 1 : map (+2) odds

pHelper _ 1 = []
pHelper (x:xs) y | rem y x == 0 = x : pHelper (x:xs) (div y x)
                 | otherwise = pHelper xs y
primeFactors :: Int -> [Int]
primeFactors = pHelper primes

