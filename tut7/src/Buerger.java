public class Buerger
{
	private final String name;
	
	public Buerger(final String name)
	{
		this.name = name;
	}
	
	@Override
	public String toString()
	{
		return name;
	}
	
	public boolean hatDiebesgut()
	{
		return false;
	}
	
	public void aktion(Buerger buerger[])
	{
	}
}
