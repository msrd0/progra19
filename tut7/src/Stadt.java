public class Stadt
{
	private Buerger buerger[];
	
	public Stadt(int anzahl)
	{
		buerger = new Buerger[anzahl];
		for (int i = 0; i < anzahl; i++)
		{
			int zufall = Zufall.zahl(4);
			switch (zufall) {
				case 0:
					buerger[i] = new Dieb(Zufall.name());
					break;
				case 1:
					buerger[i] = new Gefangener(Zufall.name());
					break;
				case 2:
					buerger[i] = new ReicherBuerger(Zufall.name(), Zufall.zahl(998) + 2);
					break;
				case 3:
					buerger[i] = new Polizist(Zufall.name());
			}
		}
	}
	
	public static void main(String args[])
	{
		Stadt stadt = new Stadt(10);
		for (int i = 0; i < 10; i++)
		{
			int zufall = Zufall.zahl(10);
			stadt.buerger[zufall].aktion(stadt.buerger);
		}
	}
}
