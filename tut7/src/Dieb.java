public class Dieb extends Buerger
{
	private int diebesgut;
	
	public Dieb(String name)
	{
		super(name);
		diebesgut = 0;
	}
	
	@Override
	public boolean hatDiebesgut()
	{
		return (diebesgut > 0);
	}
	
	@Override
	public void aktion(Buerger buerger[])
	{
		for (int i = 0; i < 5; i++)
		{
			int zufall = Zufall.zahl(buerger.length);
			if (buerger[zufall] instanceof ReicherBuerger)
			{
				ReicherBuerger rb = (ReicherBuerger)buerger[zufall];
				int klauen = Zufall.zahl(rb.getReichtum());
				diebesgut += klauen;
				rb.setReichtum(rb.getReichtum() - klauen);
			}
			else if (buerger[zufall] instanceof Polizist)
			{
				return;
			}
		}
	}
}
