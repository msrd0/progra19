public class ReicherBuerger extends Buerger
{
	private int reichtum;
	
	public ReicherBuerger(String name, int reichtum)
	{
		super(name);
		this.reichtum = reichtum;
	}
	
	@Override
	public void aktion(Buerger buerger[])
	{
		int zufall = Zufall.zahl(reichtum);
		System.out.println("Buerger " + toString() + " besticht einen Politiker mit " + zufall + " Euro");
		reichtum -= zufall;
	}
	
	public int getReichtum()
	{
		return reichtum;
	}
	
	public void setReichtum(int reichtum)
	{
		this.reichtum = reichtum;
	}
}
