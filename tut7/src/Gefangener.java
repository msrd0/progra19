public class Gefangener extends Dieb
{
	public Gefangener(String name)
	{
		super(name);
	}
	
	@Override
	public boolean hatDiebesgut()
	{
		return false;
	}
	
	@Override
	public void aktion(Buerger buerger[])
	{
		System.out.println("Gefangener " + toString() + " aergert sich im Gefaengnis");
	}
}
