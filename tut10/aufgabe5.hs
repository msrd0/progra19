fib :: Int -> Int
fib 0 = 0
fib 1 = 1
fib n = fib (n-2) + fib (n-1)

primeTest :: Int -> Int -> Bool
primeTest _ 1 = True
primeTest x y | (rem x y == 0) = False
              | otherwise = primeTest x (y-1)

prime :: Int -> Bool
prime x = primeTest x (x-1)

powersOfTwo :: Int -> Int -> [Int]
powersOfTwo a b | a > b = []
                | otherwise = (2^a) : (powersOfTwo (a+1) b)

contains :: Int -> [Int] -> Bool
contains _ [] = False
contains x (y:ys) | x == y = True
                  | otherwise = contains x ys

intersection :: [Int] -> [Int] -> [Int]
intersection [] _ = []
intersection (x:xs) ys | contains x ys = x:(intersection xs ys)
                       | otherwise = intersection xs ys

selectSmallest :: [Int] -> Int -> Int
selectSmallest [] y = y
selectSmallest (x:xs) y | x < y = selectSmallest xs x
                        | otherwise = selectSmallest xs y

removeFirst :: [Int] -> Int -> Bool -> [Int]
removeFirst [] _ _ = []
removeFirst xs _ True = xs
removeFirst (x:xs) y False | x == y = removeFirst xs y True
                           | otherwise = x:(removeFirst xs y False)

selectKsmallest :: Int -> [Int] -> Int
selectKsmallest 1 (x:xs) = selectSmallest xs x
selectKsmallest k (x:xs) = selectKsmallest (k-1) (removeFirst (x:xs) (selectSmallest xs x) False)
